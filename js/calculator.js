/* jednostavan kalkulator */

/* funkcija sabiranja */
function sabiranje(){
	var a = parseInt(document.getElementById("first").value);
	var b = parseInt(document.getElementById("second").value);
	var c = a + b;
	return alert("Rezultat je " + c); 
}
/* funkcija oduzimanja */
function oduzimanje(){
	var a = parseInt(document.getElementById("first").value);
	var b = parseInt(document.getElementById("second").value);
	var c = a - b;
	return alert("Rezultat je " + c); 
}
/* funkcija mnozenja */
function mnozenje(){
	var a = parseInt(document.getElementById("first").value);
	var b = parseInt(document.getElementById("second").value);
	var c = a * b;
	return alert("Rezultat je " + c); 
}
/* funkcija deljenja */
function deljenje(){
	var a = parseInt(document.getElementById("first").value);
	var b = parseInt(document.getElementById("second").value);
	var c = a / b;
	return alert("Rezultat je " + c); 
}
/* funkcija kvadrat */
function kvadrat(){
	var a = parseInt(document.getElementById("first").value);
	var c = a * a;
	return alert("Rezultat je " + c); 
}
/* funkcija kub */
function kub(){
	var a = parseInt(document.getElementById("first").value);
	var c = a * a * a;
	return alert("Rezultat je " + c); 
}


/* komplikovaniji kalkulator */

/* variabla za proveru dodavanja brojeva */
var appendDigits = false;

/* definisanje prethodnog rezultata */
var prevResult = 0;

/* definisanje default operacije */
var op = "=";

/* ciscenje prikaza i resetovanje variabli */
function clearDis()
{
  document.getElementById("prikaz").value = "";
  prevResult = 0;
  appendDigits = false;
  op = "=";
}

/* funkcija operacija nad brojevima */
function operation(newop)
{
  /* hvatanje argumenata */
  var newArg = eval(document.getElementById("prikaz").value);
  /* provera koja je operacija */
  if (op == "+") {
    prevResult = prevResult + newArg;
  }
  else if (op == "-") {
    prevResult = prevResult - newArg;
  } 
  else if (op == "/") {
    prevResult = prevResult / newArg;
  }
  else if (op == "*") {
    prevResult = prevResult * newArg;
  }
  else if (op == "kor") {
  	prevResult = Math.sqrt(prevResult);
  }
  else if (op == "kvadrat") {
  	prevResult = Math.pow(prevResult, 2);
  }
  else if (op == "kub") {
  	prevResult = Math.pow(prevResult, 3);
  }
  else if (op == "=") {
    prevResult = newArg;
  }
  else { 
    prevResult = newArg;
  }
  /* prikazuje rezultat i azurira variable */ 
  document.getElementById("prikaz").value = prevResult;
  appendDigits = false;
  op = newop;
}

/* dodavanje cifara na prikaz */
function digi(dig)
{
  if (appendDigits) {
    document.getElementById("prikaz").value += dig;
  }
  else {
    document.getElementById("prikaz").value = dig;
    appendDigits = true;
  }
}
